# logs-summarizer

RESTful Service for getting summary of errors given a log file.

## Requirements
- OpenJDK 11
- GNU Make
- Docker
- jq 

## Getting Started

- First, clone the repository:

```bash
git clone https://faustocv@bitbucket.org/faustocv/logs-summarizer.git
```

- Run it locally by executing the command below. The service listens in the port 8080, so you can hit http://localhost:8080/logs_summarizer and upload a log file. Alternatively,  there is an automated tasks to make it more simple. See **get_summary** task.

```bash
make run_local
```

- Run it locally through Docker by executing the command below.
```bash
make run_w_docker
```

- Get summary of a log file:

```bash
make get_summary FILE_PATH=<log-file-path>
```

## Tests

- For running them execute the command below.

```bash
make run_tests
```