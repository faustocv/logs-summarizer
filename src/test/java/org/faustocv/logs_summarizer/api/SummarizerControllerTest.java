package org.faustocv.logs_summarizer.api;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ExtendWith(MockitoExtension.class)
public class SummarizerControllerTest {

    private MockMvc mvc;

    @InjectMocks
    SummarizerController controller;

    @BeforeEach
    public void setUp() {
        mvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void shouldReturnSummaryGivenALogFile() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "log.txt", "text/plain",
                "2018-07-02T17:54:14.290Z [api-gateway ffd3082fe09d]: 2018/07/02 17:54:14 [error] Something went wrong"
                        .getBytes());
        mvc.perform(MockMvcRequestBuilders.multipart("/logs_summarizer")
                .file(file)).andExpect(status().isCreated())
                .andExpect(jsonPath("$.[0].api-gateway").value("1 errors"))
                .andExpect(jsonPath("$.[0].ffd3082fe09d").value("1/1 errors"));
    }
    
}
