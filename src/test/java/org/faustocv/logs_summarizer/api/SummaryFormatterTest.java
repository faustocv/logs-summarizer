package org.faustocv.logs_summarizer.api;

import static org.faustocv.logs_summarizer.model.LogErrorEvent.parse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.faustocv.logs_summarizer.model.LogErrorEvent;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SummaryFormatterTest {

    private LogErrorEvent apiGatewayEvent;
    private LogErrorEvent aServiceEvent;
    private Map<String, Integer> groupByServiceResults;

    @BeforeEach
    public void setUp() {
        apiGatewayEvent = parse(
                "2018-07-02T17:50:14.290Z [api-gateway ffd3082fe09d]: 2018/07/02 17:54:14 [error] Something went wrong");
        apiGatewayEvent.addUp();
        apiGatewayEvent.addUp();

        aServiceEvent = parse(
                "2018-07-02T17:50:14.290Z [a-service ffd3082fefff]: 2018/07/02 17:54:14 [error] Something went wrong");
        aServiceEvent.addUp();
        aServiceEvent.addUp();

        groupByServiceResults = new HashMap<>();
        groupByServiceResults.put("api-gateway", 2);
        groupByServiceResults.put("a-service", 3);
    }

    @Test
    public void shouldJoinAndFormatQueryResults() {
        Map<String, Optional<LogErrorEvent>> topEventByServiceResults = new HashMap<>();
        topEventByServiceResults.put(apiGatewayEvent.getServiceName(), Optional.of(apiGatewayEvent));
        topEventByServiceResults.put(aServiceEvent.getServiceName(), Optional.of(aServiceEvent));

        Map<String, String> apiGatewayItem = new HashMap<>();
        apiGatewayItem.put("api-gateway", "2 errors");
        apiGatewayItem.put("ffd3082fe09d", "2/2 errors");

        Map<String, String> aServiceItem = new HashMap<>();
        aServiceItem.put("a-service", "3 errors");
        aServiceItem.put("ffd3082fefff", "2/3 errors");

        List<Map<String, String>> actualSummary = new SummaryFormatter(groupByServiceResults, topEventByServiceResults).does();

        MatcherAssert.assertThat(actualSummary, Matchers.hasItem(apiGatewayItem));
        MatcherAssert.assertThat(actualSummary, Matchers.hasItem(aServiceItem));
    }

    @Test
    public void shouldJoinWithAtLeastServiceGroupingWhenTheTopEventOneFails() {
        Map<String, Optional<LogErrorEvent>> topEventByServiceResults = new HashMap<>();
        topEventByServiceResults.put(apiGatewayEvent.getServiceName(), Optional.of(apiGatewayEvent));
        topEventByServiceResults.put(aServiceEvent.getServiceName(), Optional.ofNullable(null));

        Map<String, String> apiGatewayItem = new HashMap<>();
        apiGatewayItem.put("api-gateway", "2 errors");
        apiGatewayItem.put("ffd3082fe09d", "2/2 errors");

        Map<String, String> aServiceItem = new HashMap<>();
        aServiceItem.put("a-service", "3 errors");

        List<Map<String, String>> actualSummary = new SummaryFormatter(groupByServiceResults, topEventByServiceResults).does();

        MatcherAssert.assertThat(actualSummary, Matchers.hasItem(apiGatewayItem));
        MatcherAssert.assertThat(actualSummary, Matchers.hasItem(aServiceItem));
    }
    
}
