package org.faustocv.logs_summarizer.model;

import static org.faustocv.logs_summarizer.model.LogErrorEvent.parse;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LogRepositoryTest {

    private LogErrorEvent eventOne;
    private LogErrorEvent eventTwo;
    private LogErrorEvent eventThree;
    private LogErrorEvent eventFour;
    private LogErrorEvent eventFive;
    private LogRepository repository;

    @BeforeEach
    public void setUp() {
        repository = new LogRepository();
        eventOne = parse(
                "2018-07-02T17:50:14.290Z [api-gateway ffd3082fe09d]: 2018/07/02 17:54:14 [error] Something went wrong");
        eventTwo = parse(
                "2018-07-02T17:51:14.290Z [api-gateway ffd3082fe09d]: 2018/07/02 17:54:14 [error] Something went wrong");
        eventThree = parse(
                "2018-07-02T17:52:14.290Z [a-service ffd3082fe09f]: 2018/07/02 17:54:14 [error] Something went wrong");
        eventFour = parse(
                "2018-07-02T17:53:14.290Z [a-service ffd3082fe09f]: 2018/07/02 17:54:14 [error] Something went wrong");
        eventFive = parse(
                "2018-07-02T17:53:14.290Z [a-service ffd3082fe09d]: 2018/07/02 17:54:14 [error] Something went wrong");
    }

    @Test
    public void shouldAddNewEventAndIncrementCount() {
        repository.add(eventOne);
        LogErrorEvent event = repository.getById(eventOne.getId());
        assertThat(event, equalTo(eventOne));
        assertThat(event.getOcurrences(), equalTo(1));
    }

    @Test
    public void shouldNotAddNewEventIfItsNull() {
        repository.add(null);
        assertThat(repository.getAll().size(),  Matchers.is(0));
    }

    @Test
    public void ifAlreadyExistEventShouldTurnUpCountWhileAddingIt() {
        repository.add(eventOne);
        repository.add(eventTwo);
        LogErrorEvent event = repository.getById(eventOne.getId());
        assertThat(event, equalTo(eventOne));
        assertThat(event.getOcurrences(), equalTo(2));
    }

    @Test
    public void shouldGetAmountOfErrorEventsByServiceName() {
        repository.add(eventOne);
        repository.add(eventTwo);
        repository.add(eventThree);
        repository.add(eventFour);
        repository.add(eventFive);
        Map<String, Integer> expectedResult = new HashMap<>();
        expectedResult.put("api-gateway", 2);
        expectedResult.put("a-service", 3);

        Map<String, Integer> actualResult = repository.groupByService();

        assertThat(actualResult.entrySet(), equalTo(expectedResult.entrySet()));
    }

    @Test
    public void shouldGetTopOneLogErrorEvents() {
        repository.add(eventOne);
        repository.add(eventTwo);
        repository.add(eventThree);
        repository.add(eventFour);
        repository.add(eventFive);
        Map<String, Optional<LogErrorEvent>> expectedResult = new HashMap<>();
        expectedResult.put(eventOne.getServiceName(), Optional.of(eventOne));
        expectedResult.put(eventThree.getServiceName(), Optional.of(eventThree));

        Map<String, Optional<LogErrorEvent>> actualResult = repository.topEventByService();

        assertThat(actualResult.entrySet(), equalTo(expectedResult.entrySet()));
    }
    
}
