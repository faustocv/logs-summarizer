package org.faustocv.logs_summarizer.model;

import static org.faustocv.logs_summarizer.model.LogErrorEvent.parse;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

public class LogErrorEventTest {

    @Test
    public void shouldParseStringIntoLogErrorEvent() {
        LogErrorEvent actualEvent = parse(
                "2018-07-02T17:54:14.290Z [api-gateway ffd3082fe09d]: 2018/07/02 17:54:14 [error] Something went wrong");

        assertThat(actualEvent.getServiceName(), is("api-gateway"));
        assertThat(actualEvent.getInstanceId(), is("ffd3082fe09d"));
        assertThat(actualEvent.getId(), is("api-gateway ffd3082fe09d"));
    }

    @Test
    public void shouldRetunNullIfItsNotParsableAsErrorEvent() {
        LogErrorEvent event = parse("2018-07-02T17:54:14.290Z [api-gateway ffd3082fe09d]: 2018/07/02 17:54:14 [info] All is going OK");
        assertThat(event, Matchers.nullValue());
    }

    @Test
    public void shouldBeTrueIfTwoEventsBelongToTheSameServiceAndInstance() {
        LogErrorEvent eventOne = parse(
                "2018-07-02T17:54:14.290Z [api-gateway ffd3082fe09d]: 2018/07/02 17:54:14 [error] Something went wrong");
        LogErrorEvent eventTwo = parse(
                "2018-07-10T17:00:14.290Z [api-gateway ffd3082fe09d]: 2018/07/10 17:00:14 [error] Something went wrong again");

        assertThat(eventOne.equals(eventTwo), Matchers.is(true));
    }

    @Test
    public void shouldBeFalseIfTwoEventsBelongToDifferentServices() {
        LogErrorEvent eventOne = parse(
                "2018-07-02T17:54:14.290Z [new-service ffd3082fe09d]: 2018/07/02 17:54:14 [error] Something went wrong");
        LogErrorEvent eventTwo = parse(
                "2018-07-10T17:00:14.290Z [api-gateway ffd3082fe09d]: 2018/07/10 17:00:14 [error] Something went wrong again");

        assertThat(eventOne.equals(eventTwo), Matchers.is(false));
    }

    @Test
    public void shouldGetItsStringRepresentationBasedOnAttributes() {
        LogErrorEvent event = parse(
                "2018-07-02T17:54:14.290Z [a-service abdcefg]: 2018/07/02 17:54:14 [error] Something went wrong");

        assertThat(event.toString(), is("a-service abdcefg"));
    }

    @Test
    public void shouldIncreaseOneUnitIfItsCalled() {
        LogErrorEvent event = parse(
                "2018-07-02T17:54:14.290Z [a-service abdcefg]: 2018/07/02 17:54:14 [error] Something went wrong");
        event.addUp();

        assertThat(event.getOcurrences(), is(1));
    }
}
