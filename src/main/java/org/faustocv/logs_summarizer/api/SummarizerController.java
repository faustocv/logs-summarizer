package org.faustocv.logs_summarizer.api;

import static org.faustocv.logs_summarizer.model.LogErrorEvent.parse;
import static org.springframework.http.HttpStatus.CREATED;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

import org.faustocv.logs_summarizer.model.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class SummarizerController {

    @Autowired
    private LogRepository repository;

    @PostMapping("/logs_summarizer")
    @ResponseStatus(CREATED)
    public List<Map<String, String>> processFile(@RequestParam("file") MultipartFile file) throws IOException {
        InputStreamReader inputReader = new InputStreamReader(file.getInputStream());
        BufferedReader buffer = new BufferedReader(inputReader);
        String line;
        while ((line = buffer.readLine()) != null) {
            repository.add(parse(line));
        }
        return new SummaryFormatter(repository.groupByService(), repository.topEventByService()).does();
    }

} 