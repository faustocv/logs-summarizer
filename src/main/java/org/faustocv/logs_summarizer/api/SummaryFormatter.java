package org.faustocv.logs_summarizer.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.faustocv.logs_summarizer.model.LogErrorEvent;

public class SummaryFormatter {

	private Map<String, Integer> groupByServiceResults;
    private Map<String, Optional<LogErrorEvent>> topEventByServiceResults;

    public SummaryFormatter(Map<String, Integer> groupByServiceResults,
            Map<String, Optional<LogErrorEvent>> topEventByServiceResults) {
        this.groupByServiceResults = groupByServiceResults;
        this.topEventByServiceResults = topEventByServiceResults;
	}

	public List<Map<String, String>> does() {
        List<Map<String, String>> results = new ArrayList<>();
        this.groupByServiceResults.entrySet().forEach(e -> { 
            Map<String, String> item = new HashMap<>();
            item.put(e.getKey(), e.getValue() + " errors"); 

            if (this.topEventByServiceResults.get(e.getKey()).isPresent()) {
                LogErrorEvent event = this.topEventByServiceResults.get(e.getKey()).get();
                item.put(event.getInstanceId(), event.getOcurrences() + "/" + e.getValue() + " errors");
            }

            results.add(item);
        });
		return results;
	}
    
}
