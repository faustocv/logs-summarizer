package org.faustocv.logs_summarizer.model;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.maxBy;
import static java.util.stream.Collectors.summingInt;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Component;

@Component
public class LogRepository {

	private Map<String, LogErrorEvent> events;

	public LogRepository() {
		this.events = new HashMap<>();
	}

	public Map<String, Integer> groupByService() {
		return this.events.values().stream()
				.collect(groupingBy(LogErrorEvent::getServiceName, summingInt(LogErrorEvent::getOcurrences)));
	}

	public Map<String, Optional<LogErrorEvent>> topEventByService() {
		return this.events.values().stream()
				.collect(groupingBy(LogErrorEvent::getServiceName, maxBy(comparingInt(LogErrorEvent::getOcurrences))));
	}

	public void add(LogErrorEvent event) {
		if(event == null) {
			return;
		}

		LogErrorEvent actual = getById(event.getId());

		if(actual != null) {
			actual.addUp();
			this.events.put(actual.getId(), actual);
		} else {
			event.addUp();
			this.events.put(event.getId(), event);
		};
	}

	public LogErrorEvent getById(String id) {
		return this.events.get(id);
	}

	public Collection<LogErrorEvent> getAll() {
		return this.events.values();
	}

}
