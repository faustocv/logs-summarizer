package org.faustocv.logs_summarizer.model;

import static java.util.regex.Pattern.compile;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogErrorEvent {
    private static final int SERVICE_INSTANCE_GROUP = 2;
    private static final String LOG_ERROR_EVENT_PATTERN = "(?<datetime>.+)(?<serviceinstance>\\[.+\\s.+\\]:)(?<logtrace>.+\\[error\\].+)";
    private static final Pattern PATTERN = compile(LOG_ERROR_EVENT_PATTERN);

    private String serviceName;
    private String instanceId;
    private Integer occurrences;

    private LogErrorEvent(String serviceName, String instanceId) {
        this.serviceName = serviceName;
        this.instanceId = instanceId;
        this.occurrences = 0;
    }

	public String getId() {
		return this.serviceName + " " + this.instanceId;
	}

    public String getInstanceId() {
        return instanceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public static LogErrorEvent parse(String inputLine) {
        Matcher matcher = PATTERN.matcher(inputLine);
        if (matcher.matches()) {
            String[] values = matcher.group(SERVICE_INSTANCE_GROUP).replace("[","").replace("]:", "").split(" ");
            return new LogErrorEvent(values[0], values[1]);
        }
        return null;
    }

    @Override
    public boolean equals(Object obj) {
        LogErrorEvent incoming = (LogErrorEvent) obj;
        if (this.getId().equals(incoming.getId())) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return getId();
    }

	public Integer getOcurrences() {
		return this.occurrences;
	}

	public void addUp() {
        this.occurrences++;
	}

}
