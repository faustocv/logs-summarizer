package org.faustocv.logs_summarizer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogsSummarizerApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogsSummarizerApplication.class, args);
	}

}
