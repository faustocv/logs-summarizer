FILE_PATH="./logs.txt"
HOSTNAME=localhost
DOCKERHUB_USERNAME = faustoc
IMAGE_REPOSITORY = $(DOCKERHUB_USERNAME)/log_summarizer

run_tests:
	./gradlew cleanTest test

run_local:
	SPRING_PROFILES_ACTIVE=local ./gradlew bootRun

run_w_docker: 
	docker container run -d --name log_summarizer \
	    -e SPRING_PROFILES_ACTIVE=local \
		-p 8080:8080 \
		$(IMAGE_REPOSITORY):latest 

get_summary:
	curl -F "file=@${FILE_PATH}" http://${HOSTNAME}:8080/logs_summarizer | jq .

build_jar:
	./gradlew clean bootJar

build_image: build_jar
	docker build -t $(IMAGE_REPOSITORY):latest -f docker/Dockerfile .

sign_in_docker:
	@docker login --username=$(DOCKERHUB_USERNAME) --password=$(DOCKERHUB_TOKEN)

tag_image_version:
	docker tag $(IMAGE_REPOSITORY):latest $(IMAGE_REPOSITORY):$(image_version)

push_image:
	docker push $(IMAGE_REPOSITORY):$(image_version)
	docker push $(IMAGE_REPOSITORY):latest